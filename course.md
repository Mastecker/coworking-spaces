# „Co-Working und Maker-Spaces“
## …sind offene Räume für alle freischaffenden, kreativen und klugen Köpfe.
### Was sind Co-Working-Spaces?
Co-Working bedeutet aus dem Englischen übersetzt „zusammen arbeiten“ oder „kollaborativ arbeiten“. Sogenannte „Co-Working-Spaces“ stellen meist große, offene Räume zur Verfügung, in denen Arbeitsplätze und eine bestehende Infrastruktur (WLAN, Drucker, Scanner, Telefon, Präsentationsprogramme, Beamer, Besprechungsräume u.v.m.) von jedem genutzt werden kann. Co-Working Spaces ermöglichen die Bildung einer Community, in der man gemeinsam an Projekten arbeiten, sich inspirieren und neu entfalten kann.
Auf der Seite  https://www.coworking.de ist ein Verzeichnis über alle eingetragenen Co-Working Spaces in Deutschland einzusehen. Die meisten eingetragenen Co-Working Spaces sind in Berlin, Hamburg, München und Frankfurt zu finden. Aber auch in kleineren, z.T. Universitätsstädten sind Co-Working Places entstanden.
Es bestehen derzeit mehr als 200 Co-Working Places in Deutschland, Tendenz steigend. 
Die Zielgruppe von Co-Working-Spaces ist buntgemischt und reicht von kreativen, offenen Köpfen, Studenten, Doktoranden, Start-up Unternehmen, Freiberufler, kleine Firmen bis hin zu Mitarbeitern und Kunden von größeren Firmen.

### Welche Existenzgründe stecken dahinter und welche Vorteile gibt es?
Co-Working-Spaces bieten eine soziale Plattform: Eine Community, die für alle einen Nutzen mit sich bringt, kann und soll entstehen Sie bieten eine professionelle Arbeitsumgebung, ein inspirierendes Arbeitsumfeld für einen kreativen Austausch. Das Ziel ist es gemeinsam zu arbeiten, Erfolge und Miss-erfolge zu teilen und gemeinsam zu wachsen.
Sie bieten eine gute Infrastruktur mit Arbeitsplätzen, WLAN, Telefon, Drucker, Scanner, Beamer, Whiteboards, u.v.m. Zudem meist Einzelarbeitsplätze, Schreibtische, Besprechungsräume, Lounge, oftmals ein Innenhof/Garten/Draußen-Arbeitsplätze. Zudem weisen sie eine gute Lage auf, inmitten von Cafés, Bars und einer guten Verkehrsanbindung.

### Wie finanzieren sich Co-Working-Spaces?
Die meisten Co-Working Places verdienen ihr Geld durch Mitgliederbeiträge. Ein Arbeitsplatz in einem Co-Working Place ist nicht kostenlos. Es wird eine Mietgebühr genommen, deren Höhe davon abhängt, ob es sich um einen Einzel- oder Gruppenarbeitsplatz, einen eigenen Büroraum oder einen Bespre-chungsraum handelt. Die Miete eines solchen Arbeitsplatzes kann nur für ein paar Stunden, einen Tag, eine Woche oder einen ganzen Monat erfolgen. Dafür bieten die Co-Working Places eine professionelle Ausstattung, WLAN und viele andere technische Vorzüge, die finanziert werden müssen. Eine weitere Einnahmequelle bieten spezielle Zusatzangebote, die auf die Mitglieder zugeschnitten sind. Sie finanzieren sich oftmals durch ein angegliedertes Café (Co-Working Spaces, die kein eigenes Café haben, bieten z.T. eine Küche an, in der Ge-tränke und kleine Snacks zubereitet werden können), Workshops, Seminare, Veranstaltungen, Community Yoga (z.B. „Hafven“ in Hannover) oder Foto- und Filmstudios (z.B. das „Kreativ Haus“ in Berlin).

### Und MakerSpaces?
MakerSpaces, auch offene Werkstatt oder FabLabs (engl. fabrication laboratory) genannt, sind offene Räumlichkeiten, in denen Privatpersonen Zugang zu modernen Produktionsverfahren haben. 
Am meisten vertreten sind Verfahren mit dem 3D-Drucker, Laser-Cutter, CNC-Maschinen, Fräser.
So ermöglichen MakerSpaces auch Privatpersonen Zugang zu solchen Fertigungsverfahren, um Ersatz- oder Einzelteile herzustellen, obwohl ein Zugang zu diesen Verfahren im Normalfall sehr schwierig wäre. 
Es gibt nicht nur MakerSpaces im Metall- oder Baubereich, sondern auch im Gestalterischen. Zum Beispiel gibt es in Berlin ein MakerSpace/Co-Working Space für Hobbynäher aber auch Modedesigner (Nadelwald co-sewing space). Sog. Repair-Cafés stellen eine Art Werkstatt zur Selbsthilfe dar, in denen Privatpersonen Hilfe bekommen oder selbst tätig werden können.

#### Beispiele
- Co-Working Space in Göttingen
https://www.prooffice.de/artikel/coworking-by-pro-office-ab-maerz-2017-in-goettingen.html
 
- Theoretische Ausarbeitung für die Stadtbibliothek Göttingen
http://d-nb.info/1097572277/34
 
- 7 Dinge über MakerSpaces
https://net.educause.edu/ir/library/pdf/eli7095.pdf

#### Quellen
- https://www.coworking.de
